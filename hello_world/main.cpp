#include <iostream>
#include <cassert>

#include "hello_world.h"

int main() {
  assert(hello_world() == "Hello, World!");
  std::cerr << "Tests passed!" << std::endl;
  return 0;
}
