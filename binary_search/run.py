#!/usr/bin/env python3

import subprocess
import sys
import threading

class SubprocessThread(threading.Thread):
    def __init__(self,
                 args,
                 stdin_pipe,
                 stdout_pipe,
                 stderr_pipe):
        threading.Thread.__init__(self)
        self.proc = subprocess.Popen(
            args,
            stdin=stdin_pipe,
            stdout=stdout_pipe,
            stderr=stderr_pipe)

    def run(self):
        try:
            self.return_code = self.proc.wait(timeout=600)
            self.stdout = "" if self.proc.stdout is None else self.proc.stdout.read()
            self.stderr = "" # if self.proc.stderr is None else self.proc.stderr.read()
        except (SystemError, OSError):
            self.return_code = -1
            self.stdout = ""
            self.stderr = "The process crashed or produced too much output."
        except subprocess.TimeoutExpired:
            self.return_code = -2
            self.stdout = ""
            self.stderr = "The process timed out"


solution = "./solution"
checker = "./checker"


"""
How to run: `python3 run.py stl small_1_input.txt`
"""

def main():
    solutionThread = SubprocessThread(
        [solution, sys.argv[1]],
        stdin_pipe=subprocess.PIPE,
        stdout_pipe=subprocess.PIPE,
        stderr_pipe=open("solution.err", "w"))
    checkerThread = SubprocessThread(
        [checker, sys.argv[2]],
        stdin_pipe=solutionThread.proc.stdout,
        stdout_pipe=solutionThread.proc.stdin,
        stderr_pipe=sys.stderr)

    solutionThread.start()
    checkerThread.start()
    solutionThread.join()
    checkerThread.join()

    print("Checker return code:", checkerThread.return_code)
    print("Solution return code:", solutionThread.return_code)


if __name__ == "__main__":
    main()

